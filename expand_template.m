function expand_template(input, output)
  configuration('reset');

  if nargin < 1
    input=get_config('default_input_name');
  end

  [inp, msg] = fopen(input, 'r');

  if inp < 0
    error(['Could not open file "' input '" in input: ' msg])
  end

  header = fgetl(inp);

  die_if_bad_header(header);

  if nargin < 2
    output=de_templatizza(input);
    default_output_name = 1;
  else
    default_output_name = 0;
  end

  make_backup_if_needed(output, default_output_name);
  
  out = fopen(output, 'w');

  if out < 0
    error(['Could not open file "' output '" in output: ' msg])
  end

  line_no = 0;
  
  while 1
    line = fgetl(inp);
    line_no = line_no + 1;
    
 % line is usually a string, but if we hit EOF fgetl returns -1
 % Therefore "isnumeric" works as "at_EOF" in octave/matlab

    if isnumeric(line)
      break
    end

    try
      expanded = do_expansion(line);
    catch
      error(['Error while processing line number ' ...
	       sprintf("%d (%s): ", line_no, line)  ...
	       lasterror.message, ...
	     sprintf(' %d', lasterror.stack.line)]);
    end
    
    fprintf(out, '%s\n', expanded);
  end

  fclose(inp);
  fclose(out);
end

function result=expand_simple_vars(line)
  pattern = '#(?<varname>[A-Za-z][A-Za-z0-9_]*(%[-+_a-zA-Z0-9.]+)?)';

  while 1
    [tokens, from, to]=regexp(line, ...
			      pattern, ...
			      'names', ...
			      'start', ...
			      'end', ...
			      'once');

    if isempty(from)
      result=line;
      return
    end

    head = line(1:from-1);
    tail = line(to+1:end);
    body = ['<&?' tokens.varname '&>'];
    line = [head, body, tail];
  end
endfunction

function result=do_expansion(line)
  line=expand_simple_vars(line);
  
  while 1
    [tokens, from, to]=regexp(line, ...
			      '<&(?<type>[!$?<%=])(?<body>[^&]*)&>', ...
			      'names', ...
			      'start', ...
			      'end', ...
			      'once');

    if isempty(from)
      result=line;
      return
    end

    head = line(1:from-1);
    tail = line(to+1:end);

    command_type = tokens.type;
    body = tokens.body;

    switch command_type
      case '='
	set_configuration_variable(body);
	replacement='';
	
      case '%'
	replacement='';
	
      case {'?', '#'}
	parts = regexp(body, ...
		       '^(?<command>[^%]+)(?<format>%.*)?$', ...
		       'names');

	replacement = to_string(evalin('caller', parts.command), parts.format);

	if strcmp(command_type, '#')
	  replacement = full_latex(parts.command, replacement);
	end

      case '!'
	evalin('caller', body);
	
	replacement = '';

      case '<'
	filename = body;

	commands = parse_data_file(filename);

	for k=1:length(commands)
	  evalin('caller', commands{k});
	end

	replacement='';
      otherwise
	error('Bad directive type %s', command_type);
    end

    line = [head, replacement, tail];
  end
end

function body=full_latex(command, result)
  tokens=regexp(command, ...
		"^ *([a-z0-9A-Z]+)(_([a-z0-9A-Z]+))? *=", ...
		"tokens");

  if isempty(tokens)
    error("Bad command in #-line: '%s'", command);
  end

  tokens=tokens{1};

  switch length(tokens)
    case 1
      var_name = tokens{1};

    case 3
      var_name = [ tokens{1} "_{" tokens{3} "}"];

    otherwise
      error("I should never arrive here");
  end
  
  body=["$" var_name "=$" result];
end


function set_configuration_variable(body)
  [from, parts] = regexp(body, ...
			 '^(?<name>[^:]+):(?<value>.*)$',
			 'start',
			 'names');

  if isempty(from)
    error(['Bad config line "' body '"']);
  end

  set_config(strtrim(parts.name), parts.value);
end

function die_if_bad_header(header)
  if index(header, '<-- TEMPLATE -->') == 0
    error('Missing template header line.  Maybe you swapped the arguments?')
  end
end

function output=de_templatizza(input)
  pos=index(input, '.tmpl');

  if pos == 0
    pos=index(input, '-tmpl');
  end

  if pos == 0
    error('No ".tmpl" or "-tmpl" part found in input filename')
  end

  output=input;
  output(pos:pos+4)=[];
end

function make_backup_if_needed(output, default_name)
  entry = dir(output);

  if isempty(entry)
    return
  end

  backup = [output '.bak'];

  if default_name == 0
    msg=['WARNING: Output file "'  output '" exists. Proceed? (y/N)'];

    ans=input(msg, 's');
    
    if strcmp(tolower(ans), 'y') == 0
      error('Aborted')
    end
  end

  disp(['Output file "'  output '" exists. Making a backup in ' backup])
  copyfile(output, backup);
end

  
function commands=parse_data_file(filename)
  [fid, msg] = fopen(filename, 'r');

  if fid < 0
    error(['Could not open file "' filename '" in input: ' msg])
  end

  searching = 0;
  in_section = 1;
  % done = 2;

  status = searching;
  commands={};
  
  while ! feof(fid)
    line = fgetl(fid);
    
    switch status
      case searching
	if is_begin_line(line)
	  status = in_section;
	end
	
      case in_section
	if is_end_line(line)
	  status = searching;
	else
	  [success, var, value] = parse_data_line(line);

	  if success
	    commands{end+1}=[var '=' value ';'];
	  end
	end

      otherwise
	error(sprintf('I should not be here: %d', status));
    end

    % if status == done
    %   break;
    % end
  end

  fclose(fid);
end

function result=is_end_line(line)
  result = ~ isempty(regexp(line, get_config('end_data_section_regexp')));
end

function result=is_begin_line(line)
  result = ~ isempty(regexp(line, get_config('begin_data_section_regexp')));
  %result = index(line, '\problemdata');
end

function [success, var, value]=parse_data_line(line)
  [from, tokens]=regexp(line, ...
			'^(?<var>[^=]+)=(?<value>[^;{]+)({[^}]*})?;', ...
			'start',			   ...
			'names');
  success=0;

  if isempty(from)
    var='';
    value='';
    return
  end
  

  var   = clear_var_name(tokens.var); %(line(1:idx-1));
  value = parse_value(tokens.value); %(line(idx+1:end));

  if isempty(var) || isempty(value)
    error('Bad data line "%s"', line)
  end

  success=1;
end

function var=clear_var_name(raw_var)
  var=remove_extra_underscore(strrep(cleanup(raw_var, '_'), " ", "_"));
end

function value=parse_value(raw_value)
  cleaned = remove_extra_underscore(strrep(cleanup(raw_value, '+-\.'), ' ', ''));
  
  tokens=regexp(cleaned, ...
		'([-+]?[0-9]+(\.[0-9]+)?) *([\\a-zA-Z]+)?', ...
		'tokens');

  tokens=tokens{1};
  
  value = [tokens{1}, to_exponent(tokens{end})];
end

function k=to_exponent(x)
  if length(x) < 2
 %
 % If a multiplier is present, x must be at least 2 character long
 %
    k='';
    return
  end

  prefix = x(1);

 %
 % Handle the special case of \mu, the only multiplier
 % with a non-letter characcer
 %
  if prefix=='\' && length(x) > 3 && strcmp(x(1:3), '\mu')
    k='e-6';
    return
  end

 %
 % Take care of the special case of 'm' which can be both a prefix
 % and a unit.  In order to be understood as a prefix it must be
 % followed by another letter (as in mV)
 %

  if prefix == 'm'
    if isalpha(x(2))
      k='e-3'
    else
      k=''
    end

    return
  end
    

  pos = index('unpkMGT', prefix);

  if pos==0
    k='';
    return
  end

  multipliers = {'e-6', 'e-9', 'e-12', 'e3', 'e6', 'e9', 'e12'};

  k = multipliers{pos};
end

function y=remove_undescore_head(x)
  idx = min(find(x ~= '_'));
  y = x(idx:end);
endfunction
  
function result=remove_extra_underscore(x)
  x = remove_undescore_head(x);

  x = fliplr(x);
  x = remove_undescore_head(x);
  x = fliplr(x);

  while 1
    old_x=x;
    x=strrep(x, "__", "_");

    if strcmp(x, old_x)
      break;
    endif
  endwhile

  result=x;
endfunction

function result=cleanup(x, extra)
  result = x;

  for k=1:length(result)
    if !(isalnum(result(k)) || index(extra, result(k)) != 0)
      result(k)=" ";
    end
  end

  result=strtrim(result);
end

function result=to_string(datum, format)
  if ischar(datum)
    result=datum;
    return
  end
  
  if isempty(format)
    format='%M';
  end

  tokens=regexp(format, ...
		'^%(?<flags>[-+ #0]*)(?<prec>[0-9]+(\.[0-9]+))?(?<type>[a-zA-Z])(?<tail>.*)$', ...
		 'names');

  if isempty(tokens)
    error(['Bad format "' format '"']);
  end

  switch tokens.type
    case 'U'
      base_fmt=['%' tokens.flags tokens.prec 'f'];

      smallest='.';
      unit = tokens.tail;

      [base, prefix] = find_prefix(datum, smallest);
      
      result=sprintf([base_fmt '~%s\\text{%s}'], base, prefix, unit);

    case 'M'
      fmt = ['%' tokens.flags  tokens.prec 'e'];
      tmp=sprintf(fmt, datum);
      
      tokens = regexp(tmp, ...
		      '^(?<num>[^e]+)e(?<mag>.+)$', ...
		      'names');

      if isempty(tokens)
	error(['This should not happen, input="', tmp '"'])
      end

      E = sscanf(tokens.mag, '%d');

      if E==0
	result=tokens.num;
      else
	result = [tokens.num sprintf('\\cdot 10^{%d}', E)];
      end
    otherwise
      result=sprintf(format, datum);
      
  end
end

function [base_fmt, smallest, unit]=parse_format(fmt)
  [tokens, from]=regexp(fmt, ...
			    '^%U(?<unit>[^%@]*)(?<tiny>@.)?(?<format>%.*)?$', ...
			    'names', ...
			    'start');

  if isempty(from)
    error(['Bad %U format "' fmt '"'])
  end

  unit=tokens.unit;

  if isempty(tokens.tiny)
    smallest='.';
  else
    smallest=tokens.tiny(2);
  end

  if isempty(tokens.format)
    base_fmt='%7.3f';
  else
    base_fmt=tokens.format;
  end
end

function [base, prefix]=find_prefix(x, smallest)
  if smallest == '.'
    smallest=0;
  else
    smallest=prefix_to_multiplier(smallest);
  end

  if abs(x) <= smallest 
    base=0;
    prefix='';
  else
    s = sign(x);
    x = abs(x);

 %
 % I am searching for n such that DATUM=V * 1000^n where 1 < V < 1000
 % This means that log10(datum) = log10(V) + n*3 where
 % 0 < log10(V) < 3.  This means n=floor(log10(V)/3)
 %
    prefix = log1000_to_prefix(floor(log10(x) / 3));
    
    base = s * x / prefix_to_multiplier(prefix);
  end
end

function prefix=log1000_to_prefix(x)
  prefixes = 'afpnum kMGTPEZY';
  first=-6; % atto=1e-18

  idx = x - first + 1;
  idx = max(idx, 1);
  idx = min(idx, length(prefixes));

  prefix=prefixes(idx);

  if strcmp(prefix, 'u')
    prefix=get_config('micro_prefix');
  end
end

function k=prefix_to_multiplier(prefix)
  prefixes = 'afpnum kMGTPEZY';
  first=-6; % atto=1e-18

  if strcmp(prefix, get_config('micro_prefix'))
    prefix='u';
  end

  if length(prefix) != 1
    error(['This should not happen (bad prefix "' prefix '")'])
  end
  
  idx = index(prefixes, prefix);

  if idx==0
    error(['This should not happen (unknown prefix "' prefix '")'])
  end

  k = 1000^((idx-1)+first);
end


function y=get_config(what)
  y=configuration('get', what);
end

function set_config(field, value)
  configuration('set', field, value);
end

function y=configuration(action, name, value)
  persistent config

  switch action
    case 'reset'
	   config= ...
	 struct('micro_prefix', '\ensuremath{\mu}', ...
		'end_data_section_regexp', '^[^%]*\\endproblemdata', ...
		'begin_data_section_regexp', '^[^%]*\\problemdata', ...
		'default_input_name', 'risposte-tmpl.tex' ...
	       );

    case 'get'
      if ! isfield(config, name)
	error(['Unknown config field "' what '"'])
      end

      y = config.(name);

    case 'set'
      if ! isfield(config, name)
	error(['Unknown config field "' what '"'])
      end

      config.(name)=value;

    otherwise
      error(['Unknown configuration command "' action '"'])
  end
end
