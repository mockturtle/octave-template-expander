[TOC]

# What is this?

This is a small Octave function that allows you to do a kind of "template expansion" replacing Octave expressions with their result in a text file.  I wrote it to work on LaTeX files, but it is usable even in other contexts.

> Why did I write this? I teach Digital Signal Processing and the exam includes a written test where the students are asked to design a simple DSP system.  The test includes, obviously, some problem data such as, say, a desired SNR or a sampling frequency or...  Clearly, every time I write a new exam text I try first to solve it by myself, to check that there is no hiddend difficulty and that the results are reasonable. Usually I write the solution in LaTeX so that I can use it as reference during grading.  This means that I need to write the solution text while doing the computation separately and copy by hand the results in the solution text.  I found this quite boring and error prone, especially if I need to  tune the problem data in order to obtain realistic results.  With this function I can, in some sense, "embed" the solution algorithm in the LaTeX text.    

## Cool, how does it work?

The function reads, line by line, the template file given as argument and searches for *template commands* that execute producing some new text that is used to replace the template command.  After the expansion (when no other template commands are in the line) the line is written to the output file.

## Installation

Just copy the file `expand_template.m` when your Octave/Matlab will find it.  That's it. Easy, isn't it?

## How to call it

You call the function in this way

```matlab
expand_template(template_filename, target_filename);
```

If a file with name `target_filename` already exists, the function asks the user to confirm the overwriting. Just to be sure, a backup copy is done by appending `.bak` to the target filename (for example, `answers.tex` is copied to `answers.tex.bak`).

> This is a safety feature: if the user exchanges the two parameters the template file risks to be overwritten. 

If the template filename includes `-tmpl` or `.tmpl` somewhere (for example, `answers-tmpl.tex` or `answers.tex.tmpl`) the function can be called with a single parameter as
```matlab
expand_template('answers-tmpl.tex');
```
or taking advantage of the special Octave/Matlab syntax

```matlab
expand_template answers-tmpl.tex
```
without parenthesis and quotes.

In the case of single parameter call, the target filename is obtained by simply deleting the string `-tmpl` or `.tmpl` from the template filename. In this way it is almost impossible to overwrite the template file; however, if the target file exists, a backup copy is done anyway. 

The single parameter call is allowed only if  the template filename includes `-tmpl` or `.tmpl`.


As a further protection from accidently overwriting files, the first line of the template file must be

```tex
% <-- TEMPLATE -->
```

This avoids to overwrite the template file in case the arguments of `expand_template` are swapped.

## A minimal example?

Look in the folder `example/`

# Syntax of the template file

### Header

Every template file must begin with a line that contains the string `<-- TEMPLATE -->`.

> This is another *safety feature* since if one calls the function with the wrong argument order, the function will not find the header in the target file. 

### Template directives

There are five types of *directives*.  Every directive begins with the character pair `<&` and ends with the character pair `&>`.  The character after the `<&` identifies the type of directive.  The directives are: 

1. Octave statement **with** replacement `<&?...&>`
1. Replacement shortcut for single variables `#...`
1. Octave statement **with** replacement and LaTeX delimiters `<&$...&>`
2. Octave statement **without** replacement `<&!...&>`
3. File inclusion `<&<filename&>`
4. Comment `<&% ... &>`
5. Configuration `<&=...&>`

Since the processing is done line-wise, **a directive cannot be split on more than one line**.

#### Directives with replacement 

This type of directive is delimited by `<&?` (or `<&$`) and `&>`.  If the text between the two delimiters has (at least) one `%`, then it is split into two parts at the first `%`, the text preceding the `%` is the actual command, the text following it is the *output format*.  If you want to be more precise and *tu hablas regexp*,  this is the regexp used to parse the text

```regexp
(?<command>[^>%]+)(?<format>%[^>]+)?
```
> Regexps like `(?<name>...)` are an extension implemented in Octave and are called *named token*.  It is a way to attach a label to a group. It is quite convenient in code and improves the readibility of regexps (and they need it....)

The first parenthesis matches the *command* part, while the second (optional) one matches the *format* which is the C format (e.g., `%g`, `%7.3f`, ...) used to "print" the result (if numeric).  In general the usual C format are accepted, with the addition of two special formats `%U` and `%M`  described in the following.

The way this directive is expanded is very simple

* The *command*  is executed
* If the result is a string, it is ueed as replacement of the directive in the original text;
* If the result is a double, it is converted to string by using the given format
* If the directive is `$`, *command* must have the form of an assignment (more precisely, it must match `"^ *([a-z0-9A-Z]+)(_([a-z0-9A-Z]+))? *="`) where the part before the `=` is a variable name. The expansion is

```tex
$\1_{\2}=expansion$
```

where `expansion` is the result of the expansion steps.  For example, `<&#F_camp=1000*2&>` is expanded to `$F_{camp}=2000`.

Note that the command is executed in the *Octave workspace* of the function.  Therefore, any side-effect (e.g. a value assigned to a variable) is preserved.  For example, in the `$` command above, after the expansion the variable `F_camp` can be used in following commands.

##### The shortcut `#...`

When the Octave expression is just a variable name, the shortcut `#...` can be used.  Expressions like, say, `#F_c%UHz` are expanded in a preliminary step as `<&?F_c%UHz&>`.  The regexp used in this case is

````
#(?<varname>[A-Za-z][A-Za-z0-9_]*(%[-+_a-zA-Z0-9.]+)?)

```
In other words, after the `#` the function expects a variable name, optionally followed by a format specifier.  Note that no space is allowed.  The named token `varname` (that is, the whole string without the initial `#`) is used as body of `<&? ... &>`.  

##### The special format `%M`

C formats `%g` and `%e` print the number in scientific notation like `1.23e+03`.  This is quite common in programming languages, but in LaTeX it feels a bit unnatural.  The special format `%M` uses a more LaTeX-like format, that is, something like 1.23 · 10<sup>3</sup>.  The format `%M` accepts the usual *flags* and *precision* used in C, so that something like `%+#5.2M` is accepted.  

##### The special format `%U`

This format prints the value using the unit prefixes like `k`, 'M`, `n', ...   followed, possibly, by a measurement unit. The syntax for this format is as follows
```
%[flags][precision]U[unit]
```

where
* `[]` denotes an optional part (therefore, only the first two characters `%U` are mandatory)
* `flags` and `precision` are interpreted in the usual way.  Therefore, something like `%-3.1U` os acceptable.
* `unit` is any sequence of characters with no `@` and it is the unit symbol to be used

> Again, *si tu parles regexp* and you want to be more precise, here it is the regexp used to parse the `%U` format 
```
^%(?<flags>[-+ #0]*)(?<prec>[0-9]+(\.[0-9]+))?U(?<unit>.*)$
```

For example, if a value is `1,200,000,000` Hz, using the `%UHz` format we get `1.200 GHz`, if we use `%4.1UHz` we get `1.2 GHz` 

The prefix for 10<sup>-6</sup> requires some special handling since it is the only prefix that is not a letter of the latin alphabet. In LaTeX one would use `\mu` (or `$\mu$` in text mode or, better yet, `\ensuremath{\mu}`), but other cases could require a different solution.  Because of this, the user can choose the string used for this prefix by setting the configuration variable `micro_prefix`.  By default `micro_prefix` is equal to `\ensuremath{\mu}`.

#### Directive without replacement.

This type of directive is delimited by `<&!` and `&>`.  The text between the delimiters is executed (still in the function workspace), but no text replaces the directive which is just deleted.  This type of directives is useful to setup the workspace.  For example

```tex
<&!a=3;&> % No replacement, assign a
<&!b=4;&> % No replacement, assign b
The hypotenuse length is $c=<&?sqrt(a^2+b^2);&>$ % The assigned values of a and b are preserved
```

#### Comment directive

This type of directive is delimited by `<&%` and `&>`.  This directive is ignored ans simply removed.  It can seems excessive to have also a comment directive, but it is useful to temporally disable other directives (it suffices to insert a `%` after the `&`).

#### Configuration
This type of directive is delimited by `<&=` and `&>`. The content has the format
```
<name>:<value>
```
where `name` is any string with no `:` in it.  Any spaces at the beginning and at the end of `name` are ignored.

#### File inclusion

In order to be sure that the answer file uses the correct problem data, I added this function that can read the data from the exam text.  See [external-file-syntax](the following section) for more details. 

# External file syntax

When including an external file the function behaves as follows

* It reads the file line by line
* It considers *only* the *block* of lines *between* the two delimiters
  - `\problemdata`
  - `\endproblemdata`
* The delimiters do not need to be alone on the line, it suffices that they are present on the line. However, they must not be preceded by a `%` (this allows to cite them in comments without triggering
the import code).
* The regexp used to match the beginning and the end of the data section are configurable: the configuration variables to set are `begin_data_section_regexp` and `end_data_section_regexp` 
* Every line in the block is expected to have the form
```
<variable> = <value>;
```
* The function is smart enough to "distillate" a valid Octave variable name from LaTeX text.  More precisely, the `<variable>` part is processed as following
  - Every non alphanumeric character is replaced with a space
  - The result is "trimmed" removing all the spaces at the beginning and at the end
  - Any sequence of two or more spaces is collapsed in a single space
  - Every remaining space is replaced with '_'
  - For example, the string `'  $\Delta f_0$ '` is processed as follows (the quotes are not part of the string, but inserted for clarity) 
    * All the non alphanumeric characters are replaced with spaces obtaining `'    Delta f_0  '`
    * The result is trimmed obtaining `'Delta f_0'`
    * Spaces are replaced with '_' obtaining `'Delta_f_0'` which is a valid variable name
* The `<value>` part also is processed in order to transform any reasonable LaTeX text into a valid number. More precisely,
  - Delete any character which is not (i) a letter **or** (ii) a digit **or** (iii) one of the following characters `-+.\/^`
  - Split the resulting string in a *numeric* and an optional *unit* part using the following regexp `([-+]?[0-9]+(\.[0-9]+)?) *([\\a-zA-Z]+)?`. The first parenthesis matches a number with an optional fractional part, the second parenthesis mathces a string of letter and backslash, the two parts can be separated by any number of spaces.
  - The beginning of the unit part is inspected to find any multipliers.  For example, if the unit part begins with `\mu` or `u` a multiplier of 10^-6 is used, if the unit begins with `k` a multiplier of 1000 is used.
  - The multiplier is appended to the numeric value to give a number expressed in scientific notation
  - For example, the string `10.5 $\mu$F` is processed as follows
    * All the undesired characters are removed to obtain `10.5 \muF`
    * The result is parsed with the regexp to obtain a numeric part equal to `10.5` and a unit part equal to `\muF`
    * The unit part begins with `\mu`, therefore `e-6` is appended to the numeric part to obtain `10.5e-6`  

> **Warning**: the procedure is "smart," but to a point. In particular the unit `m` can be mistaken for a prefix '-milli'.  The function does some checks to avoid this. For example, the unit part must be longer than one character (this takes care of the single unit meters) and the letter 'm' must be followed by another letter in order to be understood as a prefix (this takes care, for example, of `m^2`)

For example, the following line is acceptable
```tex
$\lambda_0$ = 500 nm 
```
and it is converted to the Octave statement

```matlab
lambda_0 = 500e-9
```
One can include in the test file something like
```latex
\problemdata
$\lambda_0$ = 500 nm; 
$C_0$ = 1\muF;
$t_{start}$ = 0.1s
\endproblemdata
```
and include the test file directly in the answer file in order to "import" the problem data.  This will grant that the two files are always synchronized.
> `\problemdata` is the name of a LaTeX macro defined in package `problemdata.sty` that you can find in the folder `example/`
